function mobile (mySlider) {
    $('.flexslider .media').each( function (index, element) {
        $(element).wrap('<li></li>');
    });

    $('.flexslider').each(function(index, element) {
        $(element).html().wrap("<ul class='slides'></ul>");
    });

    $(mySlider).flexslider({
        slideshow: false,
        smoothHeight: true,
        directionNav: false,
        itemWidth: 'variable',
        // On slider load, hide inactive slides, show active slide
        start: function () {
            var $flex = $('.flexslider');

            $flex.find('.slides li').hide();
            $flex.find('.flex-active-slide').show();
        },
        // After slide changed, hide inactive slides, show active slide
        after: function () {
            var $flex = $('.flexslider');

            $flex.find('.slides li').hide();
            $flex.find('.flex-active-slide').show();
        }
    });
}


function desktop () {
    $('#item-list').unwrap();
    $('#item-list .item').each( function (index, element) {
        var classList = $(element).attr('class'); 
        $(element).replaceWith($('<div class="'+classList+'" >'+$(element).html()+'</div>'));
    });

    $('#item-list').each(function(index, element) {
        $(element).replaceWith($('<div id = "item-list">'+$(element).html()+'</div>'));
    });
}