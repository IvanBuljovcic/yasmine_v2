$(function () {

	$(window).on('load', function () {
		if ($(this).outerWidth() <= 768) {
			mobileSlide ('#flex1');
			mobileSlide ('#flex2');
			mobileSlide ('#flex3');
			mobileSlide ('#flex4');
			$('.row').removeAttr('style');
		}
		else {
			$('.has-slider').removeClass('flexslider');
			rowResize();
		}
	});

	$(window).on('resize', function () {
		if ($(this).outerWidth() <= 768) {
			$('.has-slider').addClass('flexslider');
			mobileSlide ('#flex1');
			mobileSlide ('#flex2');
			mobileSlide ('#flex3');
			mobileSlide ('#flex4');
			$('.row').removeAttr('style');
		}
		else {
			desktopSlide();
			rowResize();
		}
	});
})

// GLOBAL VARIABLE for current slide
var curSlide;

// Navigation trigger
var $trigger = $('.icon.menu'),
	$nav = $('.navigation--main ul');

	$trigger.on('click', function () {
		$nav.slideToggle();
	});

// Add 'active' class on pagination. Both levels work independently
var $pagination = $('.navigation--pagination ul'),
	$pageItem = $pagination.children('li');

	$pageItem.on('click', 'a', function () {
		$parent = $(this).parents('ul'),
		$active = $parent.find('a.active');

		if ($parent.find('a').hasClass('active')) {
			$active.removeClass('active');
			$(this).addClass('active');
		}
		else {
			$(this).addClass('active');
		}
	});

/*
| Wraps the <article class="media"> into <li> elements,
| wraps the <li> elements into <ul class="slides">
|-------------------------------------------------------
| On slider start, hides all <li> elements without "flex-active-slide" class
| On slide change, shows the active slide, hides all the rest
*/
function mobileSlide (mySlider) {
	var $container = $(mySlider),
		$media = $container.find('.media'),
		$pagination = $('.flex-control-nav');

	// Removes <div class="article-wrapper">
	$container.find('.article-wrapper').contents().unwrap();

	// JUST FOR FUN
	$container.find('.media--grid-1-2.media--large').removeClass('media--large');

	// If 'media' does NOT have <li> parent && 'slides' does NOT exist, wraps the 'media' elements and creates 'slides' parent to the <li> elements
	if ($media.parents('li').length < 1 && $container.children('.slides').length < 1) {

		// Wraps <article class="media"> into <li>
		$media.each( function (index, element) {
	        $(element).wrap('<li></li>');
	    });

	    // Wraps <li> elements into <ul class="slides">
	    $container.children('li').wrapAll("<ul class='slides'></ul>");

	    // Set 'active' class on proper <li>
	    $container.find('li').eq(curSlide).addClass('flex-active-slide');
	    $('.flex-active-slide').show();
	}

	// Show pagination if hidden
	if (!$pagination.is(':visible')) {
		$pagination.show();
	}

	// Flexslider settings
    $(mySlider).flexslider({
        slideshow: false,
        directionNav: false,
        itemWidth: 'variable',
        // On slider load, hide inactive slides, show active slide
        start: function (slider) {
            var $flex = $(mySlider);

            $flex.find('.slides li').hide();
            $flex.find('.flex-active-slide').show();
            window.curSlide = slider.currentSlide;
        },
        // After slide changed, hide inactive slides, show active slide
        after: function (slider) {
            var $flex = $(mySlider);

            $flex.find('.slides li').hide();
            $flex.find('.flex-active-slide').show();
            window.curSlide = slider.currentSlide;
        }
    });
}

// Hide slider on desktop/tablet
function desktopSlide () {
	var $container = $('.has-slider');

	$container.removeAttr('style').removeClass('flexslider');
	$container.find('.slides li').contents().unwrap();
	$container.find('.slides').contents().unwrap();
	$container.find('.flex-control-nav').hide();
	$container.find('.media--grid-1-2').addClass('media--large');
}

// Resize 'row' elements to the same height; Highest 'row' is used as base height
function rowResize () {
	var $rowContainer = $('.article-wrapper__inner-wrapper'),
		$row = $rowContainer.children('.row'),
		maxHeight = 0;

	// Check each rows height; If a rows height is higher then maxHeight, maxHeight is assigned the height of the row;
	$row.each( function(index, element) {
		var $this = $(element),
			height = $this.height();

		if (height > maxHeight) {
			maxHeight = height;
		}

	});

	$row.height(maxHeight);
}